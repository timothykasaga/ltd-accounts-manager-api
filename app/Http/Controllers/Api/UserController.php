<?php

namespace App\Http\Controllers\Api;

use App\BaseResponse;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function login(Request $request){

        try{

            $this->validateLoginInput($request);
            $username = $request['username'];
            $password = $request['password'];

            $user = User::where('email',$username)->first();
            if(!isset($user)){
                return jsonResp(BaseResponse::failure("Invalid credentials supplied"));
            }

            /**
             * validate the user password supplied
             */
            if(!Hash::check($password, $user->password)){
               return jsonResp(BaseResponse::failure("Invalid credentials supplied"));
            }

            /**
             * create token for user to be used in the
             * subsequent requests. Update the user record to have this new token
             */
            $token = Str::random(80);
            $user->api_token = hash('sha256', $token); // bcrypt($token);
            $user->save();

            return jsonResp(BaseResponse::success($token));

        }catch (\Exception $exception){
            return exceptionApiResp($exception);
        }

    }

    public function register(Request $request){
        try{

            $this->validateRegistrationInput($request);

            $cleaned = $request->only(['name','email','password']);
            $cleaned['password'] = bcrypt($cleaned['password']);
            User::create($cleaned);

            return jsonResp(BaseResponse::success("Registration successfully completed"));

        }catch (\Exception $exception){
            return exceptionApiResp($exception);
        }
    }

    private function validateLoginInput(Request $request)
    {
        Validator::make($request->all(),[
            'username' => 'required|exists:users,email',
            'password' => 'required'
        ])->validate();
    }

    private function validateRegistrationInput(Request $request)
    {
        Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required'
        ])->validate();
    }

}
