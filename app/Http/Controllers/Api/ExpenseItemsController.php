<?php

namespace App\Http\Controllers\Api;

use App\BaseResponse;
use App\ExpenseItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExpenseItemsController extends Controller
{

    public function store(Request $request){
        try{
            $cleaned = $request->only(['category_id','item_code','name','unit_cost']);
            ExpenseItem::create($cleaned);
            return jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

    public function all(){
        try{
            $records = ExpenseItem::with('category')->get();
            return jsonResp(BaseResponse::success($records));
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

    public function update(ExpenseItem $item, Request $request){
        try{
            $cleaned = $request->only(['category_id','item_code','name','unit_cost']);
            foreach ($cleaned as $field => $value) $item->$field = $value;
            $item->save();
            return  jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

    public function delete(ExpenseItem $item){
        try{
            $item->delete();
            return jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

}
