<?php

namespace App\Http\Controllers\Api;

use App\BaseResponse;
use App\Expense;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ExpensesController extends Controller
{

    public function store(Request $request){
        try{

            $cleaned = $request->only(['expense_item_id','unit_cost','quantity','amount_before_discount','discount','amount','expense_date','justification']);
            $this->validateRequest($cleaned);
            Expense::create($cleaned);

            return jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
           return exceptionApiResp($exception);
        }
    }

    /**
     * @param array $cleaned
     * @return array
     * @throws ValidationException
     */
    public function validateRequest(array $cleaned): array
    {
        return Validator::make($cleaned, [
            'expense_item_id' => 'required',
            'unit_cost' => 'required',
            'quantity' => 'required',
            'amount_before_discount' => 'required',
            'discount' => 'required',
            'amount' => 'required',
            'expense_date' => 'required',
        ])->validate();
    }

    public function update(Request $request, Expense $expense){
        try{

            $cleaned = $request->only(['expense_item_id','unit_cost','quantity','amount_before_discount','discount','amount','expense_date','justification']);
            $this->validateRequest($cleaned);
            $expense->update($cleaned);

            return jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return exceptionApiResp($exception);
        }
    }

    public function delete(Expense $expense){
        try{
            $expense->delete();
            return jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return exceptionApiResp($exception);
        }
    }

    public function all(){
        try{
            $records = Expense::with(['item'])->get();
            return jsonResp(BaseResponse::success($records));
        }catch (\Exception $exception){
            return exceptionApiResp($exception);
        }
    }

    public function show(Expense $expense){
        try{
            return jsonResp(BaseResponse::success($expense));
        }catch (\Exception $exception){
            return exceptionApiResp($exception);
        }
    }

}
