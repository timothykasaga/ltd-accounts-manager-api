<?php

namespace App\Http\Controllers\Api;

use App\BaseResponse;
use App\ExpenseCategory;
use App\ExpenseItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{

    public function stats(){
        try{
            $data = array(
              'count_expense_categories' => ExpenseCategory::all()->count(),
              'count_expense_items' => ExpenseItem::all()->count(),
            );
            return jsonResp(BaseResponse::success($data));
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

}
