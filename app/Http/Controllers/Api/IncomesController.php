<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Income;
use Illuminate\Http\Request;

class IncomesController extends Controller
{

    public function all(){

        $records = Income::orderBy('id','desc')->paginate('3');
        return json_encode($records);

    }

    public function store(Request  $request){

        $data = $request->all();
        $cleaned = $request->only(['category','item','amount','date']);
        $record = Income::create($cleaned);
        return json_encode("OK");

    }

}
