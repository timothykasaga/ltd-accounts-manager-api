<?php

namespace App\Http\Controllers\Api;

use App\BaseResponse;
use App\ExpenseCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExpenseCategoryController extends Controller
{

    public function store(Request  $request){
        try{
            $data = $request->only('category_name');
            ExpenseCategory::create($data);
            return jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }


    public function all(){
        try{
            $data = ExpenseCategory::all();
            return jsonResp(BaseResponse::success($data));
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

    public function update(ExpenseCategory  $category, Request $request){
        try{
            $data = $request->only('category_name');
            $category->category_name = $data['category_name'];
            $category->save();
            return  jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

    public function show(ExpenseCategory $category){
        try{
            return  jsonResp(BaseResponse::success($category));
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

    public function delete(ExpenseCategory  $category){
        try{
            $category->delete();
            return  jsonResp(BaseResponse::success());
        }catch (\Exception $exception){
            return jsonResp(BaseResponse::failure($exception->getMessage()));
        }
    }

}
