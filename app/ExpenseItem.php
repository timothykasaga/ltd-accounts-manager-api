<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseItem extends Model
{
    protected $guarded = [];

    public function category(){
        return $this->belongsTo('App\ExpenseCategory', 'category_id','id');
    }

}
