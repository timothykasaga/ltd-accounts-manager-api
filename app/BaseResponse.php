<?php


namespace App;


class BaseResponse
{

    public $statusCode;
    public $statusDescription;
    public $result;
    public $errors;

    public static function success($data = null){
        $resp = new BaseResponse();
        $resp->statusCode = "0";
        $resp->statusDescription = "Success";
        $resp->result = $data;
        return $resp;
    }

    public static function failure($error){
        $resp = new BaseResponse();
        $resp->statusCode = "1";
        $resp->statusDescription = $error;
        return $resp;
    }

    public static function validationError($errors){
        $resp = new BaseResponse();
        $resp->statusCode = "1";
        $resp->statusDescription = 'VALIDATION_FAILED';
        $resp->errors = $errors;
        return $resp;
    }

}
