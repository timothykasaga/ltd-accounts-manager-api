<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $guarded = [];

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('M d Y, h:i:s');
    }

}
