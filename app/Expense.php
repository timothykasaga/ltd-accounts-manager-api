<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $guarded = [];

    public function item(){
        return $this->belongsTo('App\ExpenseItem', 'expense_item_id','id');
    }

}
