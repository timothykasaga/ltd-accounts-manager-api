<?php

use App\BaseResponse;
use Illuminate\Validation\ValidationException;

if(!function_exists('jsonResp')){
    function jsonResp($data){
        return json_encode($data);
    }
}

if(!function_exists('successJsonApiResp')){
    function successJsonApiResp(){
        return ['statusCode' => '0'];
    }
}

if(!function_exists('exceptionApiResp')){
    function exceptionApiResp(\Exception $exception){

        return $exception instanceof ValidationException ?
            jsonResp(BaseResponse::validationError($exception->errors())) :
            jsonResp(BaseResponse::failure($exception->getMessage()));

    }
}
