<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//incomes
Route::get('incomes','Api\IncomesController@all');
Route::post('incomes','Api\IncomesController@store');


//expense category
Route::post('expense-category','Api\ExpenseCategoryController@store');
Route::get('expense-category','Api\ExpenseCategoryController@all');
Route::get('expense-category/{category}','Api\ExpenseCategoryController@show');
Route::put('expense-category/{category}','Api\ExpenseCategoryController@update');
Route::delete('expense-category/{category}','Api\ExpenseCategoryController@delete');


//expense item
Route::post('expense-item','Api\ExpenseItemsController@store');
Route::get('expense-item','Api\ExpenseItemsController@all');
Route::put('expense-item/{item}','Api\ExpenseItemsController@update');
Route::delete('expense-item/{item}','Api\ExpenseItemsController@delete');


//admin dashboard
Route::get('admin-dashboard/stats','Api\AdminDashboardController@stats');


//expenses
Route::middleware('auth:api')->get('expense','Api\ExpensesController@all');
Route::post('expense','Api\ExpensesController@store');
Route::put('expense/{expense}','Api\ExpensesController@update');
Route::delete('expense/{expense}','Api\ExpensesController@delete');
Route::get('expense/{expense}','Api\ExpensesController@show');

//users
Route::post('users/login','Api\UserController@login');
Route::post('users','Api\UserController@register');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
