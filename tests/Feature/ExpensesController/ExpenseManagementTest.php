<?php

namespace Tests\Feature\ExpensesController;

use App\Expense;
use App\ExpenseCategory;
use App\ExpenseItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExpenseManagementTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function expenseCanBeCreated(){

        $this->withoutExceptionHandling();

        ///////
        /// pre conditions
        //////
        $this->createExpenseCategoryAndExpenseItem();

        ///////
        /// act
        ///////
        $item = ExpenseItem::first();
        $qty = 2;
        $discount = 5000;

        $response = $this->post('api/expense',[
            'expense_item_id' => $item->id,
            'unit_cost' => $item->unit_cost,
            'quantity' => $qty,
            'amount_before_discount' => ($qty * $item->unit_cost),
            'discount' => $discount,
            'amount' => ($qty * $item->unit_cost) - $discount,
            'expense_date' => '2021-09-25'
        ]);

        ///////
        /// post conditions
        //////
        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $this->assertEquals(1, Expense::all()->count());

        $expense  = Expense::first();
        $this->assertEquals((($qty * $item->unit_cost) - $discount), $expense->amount);

    }

    /**
     * @test
     */
    public function expenseCanBeUpdated(){

        $this->withoutExceptionHandling();

        ///////
        /// pre conditions
        //////

        /**
         * Create an Expense Item
         */
        $this->createExpenseCategoryAndExpenseItem();

        /**
         * Create expense
         */
        $item = ExpenseItem::first();
        $qty = 2;
        $discount = 5000;

        $this->post('api/expense',[
            'expense_item_id' => $item->id,
            'unit_cost' => $item->unit_cost,
            'quantity' => $qty,
            'amount_before_discount' => ($qty * $item->unit_cost),
            'discount' => $discount,
            'amount' => ($qty * $item->unit_cost) - $discount,
            'expense_date' => '2021-09-25'
        ]);

        $old = Expense::first();

        ///////
        /// act
        ///////
        $item = $old->item;
        $qty = 2;
        $discount = 10000;

        $response = $this->put('api/expense/'.$old->id,[
            'expense_item_id' => $item->id,
            'unit_cost' => $item->unit_cost,
            'quantity' => $qty,
            'amount_before_discount' => ($qty * $item->unit_cost),
            'discount' => $discount,
            'amount' => ($qty * $item->unit_cost) - $discount,
            'expense_date' => '2021-09-25'
        ]);

        ////////
        /// post conditions
        //////////
        $updated = Expense::first();

        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $this->assertEquals($old->id, $updated->id );
        $this->assertEquals(5000, ($old->amount - $updated->amount)); //i gave a discount of 5000 more on updated

    }

    /**
     * @test
     */
    public function expenseCanBeRemoved(){

        ///////
        /// pre conditions
        //////

        /**
         * Create an Expense Item
         */
        $this->createExpenseCategoryAndExpenseItem();

        /**
         * Create expense
         */
        $item = ExpenseItem::first();
        $qty = 2;
        $discount = 5000;

        $this->post('api/expense',[
            'expense_item_id' => $item->id,
            'unit_cost' => $item->unit_cost,
            'quantity' => $qty,
            'amount_before_discount' => ($qty * $item->unit_cost),
            'discount' => $discount,
            'amount' => ($qty * $item->unit_cost) - $discount,
            'expense_date' => '2021-09-25'
        ]);

        $this->assertEquals(1, Expense::count());

        ///////
        /// act
        ///////
        $expense = Expense::first();
        $response = $this->delete('api/expense/'.$expense->id);

        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $this->assertEquals(0, Expense::count());

    }

    /**
     * @test
     */
    public function expensesCanBeRetrieved(){

        $this->withoutExceptionHandling();

        ///////
        /// pre conditions
        //////

        /**
         * Create an Expense Item
         */
        $this->createExpenseCategoryAndExpenseItem();

        /**
         * Create expense
         */
        $item = ExpenseItem::first();
        $qty = 2;
        $discount = 5000;

        $this->post('api/expense',[
            'expense_item_id' => $item->id,
            'unit_cost' => $item->unit_cost,
            'quantity' => $qty,
            'amount_before_discount' => ($qty * $item->unit_cost),
            'discount' => $discount,
            'amount' => ($qty * $item->unit_cost) - $discount,
            'expense_date' => '2021-09-25'
        ]);

        //////
        /// act
        ///////
        $response = $this->get('api/expense');

        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $response->assertJsonCount(1, 'result');

    }

    public function createExpenseCategoryAndExpenseItem(): void
    {

        /**
         * create an expense category
         */
        $this->post('api/expense-category', [
            'category_name' => 'Black Tax'
        ]);

        /**
         * create an expense item
         */
        $this->post('api/expense-item', [
            'category_id' => ExpenseCategory::first()->id,
            'item_code' => '1001',
            'name' => 'Side Chic',
            'unit_cost' => 200000
        ]);

        $this->assertEquals(0, Expense::all()->count());

    }


}
