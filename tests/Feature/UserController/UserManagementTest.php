<?php

namespace Tests\Feature\UserController;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserManagementTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function aUserCanRegister(){

        //////
        /// pre conditions
        //////
        $this->assertEquals(0, User::count());


        /////
        /// act
        /////
        $response = $this->post('api/users',[
           'name' => 'Timothy Kasaga',
           'email' => 'timothykasaga@gmail.com',
           'password' => 'fucklove'
        ]);

        //////
        /// post conditions
        /// //
        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $this->assertEquals(1, User::count());

    }

    /**
     * @test
     */
    public function aUserCanLoginIntoTheAppAndReceiveAnAuthToken(){

        //////
        /// preconditions
        ///////
        $user = new User();
        $user->name = "Timothy Kasaga";
        $user->email = "timothykasaga@gmail.com";
        $user->password = bcrypt("fucklove");
        $user->save();

        $this->assertEquals(1, User::count());


        ////////
        /// act
        ///////
        $response = $this->post('api/users/login',[
            'username' => $user->email,
            'password' => "fucklove"
        ]);


        ///////
        /// post conditions
        ////////
        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());

    }

}
