<?php

namespace Tests\Feature\AdminDashboardController;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminDashboardTest extends TestCase
{

    use RefreshDatabase;
    /**
     * @test
     */
    public function dashboardStatsCanBeRetrieved(){

        ///////
        /// act
        ///////

        $response = $this->get('api/admin-dashboard/stats');


        ////////
        /// post conditions
        ///////

        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $response->assertJson(['result' => ['count_expense_categories' => 0, 'count_expense_items'=> 0]]);


    }

}
