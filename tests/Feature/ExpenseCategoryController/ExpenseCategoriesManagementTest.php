<?php

namespace Tests\Feature\ExpenseCategoryController;

use App\ExpenseCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Webmozart\Assert\Assert;

class ExpenseCategoriesManagementTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function anExpenseCategoryCanBeCreated(){

        ////////////////
        //pre conditions
        ////////////////
        $this->assertEmpty(ExpenseCategory::all());


        //////////////
        /// act
        /// //////////
        $response = $this->post('api/expense-category',[
           'category_name' => 'Black Tax'
        ]);


        ////////////////////
        /// post conditions
        ///////////////////
        $response->assertStatus(200);
        $this->assertEquals(1, ExpenseCategory::all()->count());
        $response->assertJson(successJsonApiResp());

    }

    /**
     * @test
     */
    public function expenseCategoriesCanBeRetrieved(){

        ////////
        /// pre conditions
        ////////
        $response = $this->post('api/expense-category',[
            'category_name' => 'Black Tax'
        ]);

        ////////////
        /// act
        ////////////
        $response = $this->get('api/expense-category');

        ////////////
        /// post conditions
        ////////////
        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $response->assertJsonCount(1, 'result');

    }

    /**
     * @test
     */
    public function anExpenseCategoryCanBeUpdated(){

        $this->withoutExceptionHandling();

        ////////
        /// pre conditions
        ////////
        $response = $this->post('api/expense-category',[
            'category_name' => 'Black Tax'
        ]);

        ////////
        /// act
        /////////
        $category = ExpenseCategory::first();
        $this->assertEquals('Black Tax', $category->category_name);

        $response = $this->put('api/expense-category/'.$category->id, [
            'category_name' =>'Black Tax Updated'
        ]);
        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());

        $category->refresh();
        $this->assertEquals('Black Tax Updated', $category->category_name);

    }

}
