<?php

namespace Tests\Feature\ExpenseItemsController;

use App\ExpenseCategory;
use App\ExpenseItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExpenseItemsManagementTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function anExpenseItemCanBeCreated(){

        ///////
        /// pre conditions
        ///////
        $response = $this->post('api/expense-category',[
            'category_name' => 'Black Tax'
        ]);

        /////////
        /// act
        //////////
        $response = $this->post('api/expense-item',[
            'category_id' => ExpenseCategory::first()->id,
            'item_code' => '1001',
            'name' => 'Side Chic',
            'unit_cost' => 200000
        ]);

        /////
        /// post conditions
        /////////////

        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $this->assertEquals(1, ExpenseItem::all()->count());

    }

    /**
     * @test
     */
    public function expenseItemsCanBeRetrieved(){

        ///////
        /// pre conditions
        ///////
        $response = $this->post('api/expense-category',[
            'category_name' => 'Black Tax'
        ]);

        $response = $this->post('api/expense-item',[
            'category_id' => ExpenseCategory::first()->id,
            'item_code' => '1001',
            'name' => 'Side Chic',
            'unit_cost' => 200000
        ]);


        //////
        /// act
        ///////
        $response = $this->get('api/expense-item');

        //////
        /// post conditions
        ///////
        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());
        $response->assertJsonCount(1, 'result');

    }

    /**
     * @test
     */
    public function anExpenseItemCanBeUpdated(){
        $this->withoutExceptionHandling();

        ///////
        /// pre conditions
        ///////
        $response = $this->post('api/expense-category',[
            'category_name' => 'Black Tax'
        ]);

        $response = $this->post('api/expense-item',[
            'category_id' => ExpenseCategory::first()->id,
            'item_code' => '1001',
            'name' => 'Side Chic',
            'unit_cost' => 200000
        ]);

        /////
        /// act
        ///////
        $item = ExpenseItem::first();
        $response = $this->put('api/expense-item/'.$item->id,[
            'category_id' => $item->id,
            'item_code' => '1001',
            'name' => 'Side Chic',
            'unit_cost' => 400000
        ]);

        $response->assertSuccessful();
        $response->assertJson(successJsonApiResp());

        $item->refresh();
        $this->assertEquals(400000, $item->unit_cost);

    }

}
